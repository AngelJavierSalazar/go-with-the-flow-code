package handlers

import (
	"net/http"

	"github.com/labstack/echo/v4"

	"hartree.stfc.ac.uk/hbaas-server/version"
)

type MaintenanceHandler struct{}

func (h MaintenanceHandler) registerEndpoints(g *echo.Group) {
	g.GET("version", h.showVersion)
}

func (h MaintenanceHandler) showVersion(c echo.Context) error {
	return c.JSON(
		http.StatusOK,
		map[string]string{
			"version":    version.Version,
			"build_time": version.BuildTime,
		},
	)
}
