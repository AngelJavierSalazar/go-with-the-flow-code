package handlers

import (
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/labstack/echo/v4"

	"hartree.stfc.ac.uk/hbaas-server/context"
)

type BirthdayHandler struct {
	Context context.Context
}

func (h BirthdayHandler) registerEndpoints(g *echo.Group) {
	g.GET("", h.sayHello)
	g.GET("name/:name", h.sayHappyBirthdayToName)
	g.GET("date/:date", h.sayHappyBirthdayByDate)
}

func (h BirthdayHandler) sayHello(c echo.Context) error {
	message := "Welcome! Try sending a request to '/name/{some-name}' to get started!"
	return c.JSON(
		http.StatusOK,
		NewAPIMessage(message),
	)
}

func (h BirthdayHandler) sayHappyBirthdayToName(c echo.Context) error {
	name := c.Param("name")
	message := fmt.Sprintf("Happy birthday %s!", name)
	return c.JSON(
		http.StatusOK,
		NewAPIMessage(message),
	)
}

func (h BirthdayHandler) sayHappyBirthdayByDate(c echo.Context) error {
	date := c.Param("date")

	dateTime, err := time.Parse("2-January", date)
	if err != nil {
		return echo.ErrBadRequest
	}

	message := "It doesn't look like I know of anyone with that birthday!"

	birthDay := context.NewBirthDay(dateTime)
	names, exists := h.Context.PeopleByBirthday[birthDay]
	if exists {
		names[len(names)-2] = fmt.Sprintf(
			"%s and %s",
			names[len(names)-2],
			names[len(names)-1],
		)
		names = names[0 : len(names)-1]
		message = fmt.Sprintf(
			"Happy birthday to %s!",
			strings.Join(names, ", "),
		)
	}

	return c.JSON(http.StatusOK, NewAPIMessage(message))
}
